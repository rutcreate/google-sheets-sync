﻿using UnityEngine;
using System.Collections;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;


public static class OfflineCertificationValidator
{
	private static bool m_IsOfflineCertificateValidationStarted = false;

	public static void Start()
	{
		if (!m_IsOfflineCertificateValidationStarted)
		{
			ServicePointManager.ServerCertificateValidationCallback += LocalhostCertificationValidator;
			m_IsOfflineCertificateValidationStarted = true;
		}
	}

	public static void Stop()
	{
		if (m_IsOfflineCertificateValidationStarted)
		{
			ServicePointManager.ServerCertificateValidationCallback -= LocalhostCertificationValidator;
			m_IsOfflineCertificateValidationStarted = false;
		}
	}

	private static bool LocalhostCertificationValidator(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors errors)
	{
		return true;
	}
}
