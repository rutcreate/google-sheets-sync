﻿using UnityEngine;
using UnityEditor;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;

using Google.GData.Client;
using Google.GData.Extensions;
using Google.GData.Spreadsheets;


public class GoogleAppCredentialsEditorWindow : EditorWindow
{
	[MenuItem("Window/Google App Credentials")]
	public static void OpenWindow()
	{
		GetWindow<GoogleAppCredentialsEditorWindow>(false, "Credentials", true);
	}

	public const string ClientIdKey = "GoogleApp_ClientID";
	public const string ClientSecretKey = "GoogleApp_ClientSecret";
	public const string RedirectUriKey = "GoogleApp_RedirectUri";
	public const string ScopeKey = "GoogleApp_Scope";
	public const string AccessCodeKey = "GoogleApp_AccessCode";
	public const string AccessTokenKey = "GoogleApp_AccessToken";
	public const string RefreshTokenKey = "GoogleApp_RefreshToken";
	public const string ApplicationNameKey = "GoogleApp_ApplicationName";
	public const string SpreadsheetIdKey = "GoogleApp_SpreadsheetID";
	public const string SkipFirstRowKey = "GoogleSheetSyncFirstRow";
	public const string SpreadsheetFeedApiUriPrefix = "https://spreadsheets.google.com/feeds/spreadsheets/private/full/";

	private bool m_AutoGetAccessCode;

	private static OAuth2Parameters m_OAuth2Parameters;
	private static GOAuth2RequestFactory m_OAuth2RequestFactory;
	private static SpreadsheetsService m_SpreadsheetsService;
	private HttpListener m_HttpListener;

	private Vector2 m_ScrollPosition;

	private void OnEnable()
	{
		LoadPrefs();
	}

	private void OnGUI()
	{
		DrawSpreadsheetCredentials();
	}

	private void DrawSpreadsheetCredentials()
	{
		m_ScrollPosition = EditorGUILayout.BeginScrollView(m_ScrollPosition);

		EditorGUILayout.BeginVertical("Box");

		m_OAuth2Parameters.ClientId = EditorGUILayout.TextField("Client Id", m_OAuth2Parameters.ClientId);
		m_OAuth2Parameters.ClientSecret = EditorGUILayout.TextField("Client Secret", m_OAuth2Parameters.ClientSecret);
		m_OAuth2Parameters.Scope = EditorGUILayout.TextField("Scope", m_OAuth2Parameters.Scope);

		// Get Access Code.
		EditorGUILayout.BeginHorizontal();
		if (!string.IsNullOrEmpty(m_OAuth2Parameters.AccessCode))
		{
			m_OAuth2Parameters.AccessCode = EditorGUILayout.TextField("Access Code", m_OAuth2Parameters.AccessCode);
		}
		else
		{
			EditorGUILayout.PrefixLabel("Access Code");
		}
		if (GUILayout.Button("Get Access Code", EditorStyles.miniButton, GUILayout.Width(120f)))
		{
			m_OAuth2Parameters.AccessType = "offline";
			m_OAuth2Parameters.AccessCode = string.Empty;
			m_OAuth2Parameters.AccessToken = string.Empty;
			m_OAuth2Parameters.RedirectUri = "http://localhost:8888/";

			m_AutoGetAccessCode = false;

			if (m_HttpListener != null && m_HttpListener.IsListening)
			{
				m_HttpListener.Close();
			}

			m_HttpListener = new HttpListener();
			m_HttpListener.Prefixes.Add("http://*:8888/");
			m_HttpListener.IgnoreWriteExceptions = true;

			m_HttpListener.Start();
			m_HttpListener.BeginGetContext((callback) =>
			{
				OfflineCertificationValidator.Start();

				callback.AsyncWaitHandle.WaitOne();
				m_AutoGetAccessCode = true;

				HttpListenerContext context = m_HttpListener.EndGetContext(callback);
				m_OAuth2Parameters.AccessCode = context.Request.QueryString["code"];
				byte[] bytes = Encoding.UTF8.GetBytes("Please close this window or tab and get back to Unity.\t");
				context.Response.ContentType = "text/html";
				context.Response.ContentLength64 = bytes.Length;
				context.Response.ContentEncoding = System.Text.Encoding.UTF8;
				context.Response.OutputStream.Write(bytes, 0, bytes.Length);
				context.Response.Close();
				m_HttpListener.Close();

				OfflineCertificationValidator.Stop();
			}, null);

			string authorizationUrl = OAuthUtil.CreateOAuth2AuthorizationUrl(m_OAuth2Parameters);
			if (string.IsNullOrEmpty(authorizationUrl))
			{
				EditorUtility.DisplayDialog("Error", "Could not get authorization URL. Please make sure your credentials are correct and try again.", "OK");
			}
			else
			{
				Application.OpenURL(authorizationUrl);
			}
		}
		EditorGUILayout.EndHorizontal();

		// Get Access Token after get Access Code.
		if (m_AutoGetAccessCode)
		{
			m_AutoGetAccessCode = false;

			OfflineCertificationValidator.Start();
			OAuthUtil.GetAccessToken(m_OAuth2Parameters);
			OAuthUtil.RefreshAccessToken(m_OAuth2Parameters);
			OfflineCertificationValidator.Stop();
		}

		// Display Access Token value.
		if (!string.IsNullOrEmpty(m_OAuth2Parameters.AccessToken))
		{
			GUI.enabled = false;
			EditorGUILayout.TextField("Access Token", m_OAuth2Parameters.AccessToken);
			GUI.enabled = true;
		}

		EditorGUILayout.EndVertical();

		EditorGUILayout.BeginHorizontal();
		if (GUILayout.Button("Save & Close", EditorStyles.miniButton))
		{
			SavePrefs();
			Close();
		}
		GUILayout.FlexibleSpace();
		if (GUILayout.Button("Clear", EditorStyles.miniButton))
		{
			if (EditorUtility.DisplayDialog("Clear Credentails", "Are you sure you want to clear all credentails? This action cannot be undo.", "OK", "Cancel"))
			{
				ClearPrefs();
			}
		}
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.EndScrollView();
	}

	#region Prefs

	private void LoadPrefs()
	{
		if (m_OAuth2Parameters == null)
		{
			m_OAuth2Parameters = new OAuth2Parameters();
		}

		m_OAuth2Parameters.ClientId = EditorPrefs.GetString(ClientIdKey);
		m_OAuth2Parameters.ClientSecret = EditorPrefs.GetString(ClientSecretKey);
		m_OAuth2Parameters.RedirectUri = EditorPrefs.GetString(RedirectUriKey);
		m_OAuth2Parameters.Scope = EditorPrefs.GetString(ScopeKey);
		m_OAuth2Parameters.AccessCode = EditorPrefs.GetString(AccessCodeKey);
		m_OAuth2Parameters.AccessToken = EditorPrefs.GetString(AccessTokenKey);
		m_OAuth2Parameters.RefreshToken = EditorPrefs.GetString(RefreshTokenKey);
	}

	private void SavePrefs()
	{
		if (m_OAuth2Parameters != null)
		{
			EditorPrefs.SetString(ClientIdKey, m_OAuth2Parameters.ClientId);
			EditorPrefs.SetString(ClientSecretKey, m_OAuth2Parameters.ClientSecret);
			EditorPrefs.SetString(RedirectUriKey, m_OAuth2Parameters.RedirectUri);
			EditorPrefs.SetString(ScopeKey, m_OAuth2Parameters.Scope);
			EditorPrefs.SetString(AccessCodeKey, m_OAuth2Parameters.AccessCode);
			EditorPrefs.SetString(AccessTokenKey, m_OAuth2Parameters.AccessToken);
			EditorPrefs.SetString(RefreshTokenKey, m_OAuth2Parameters.RefreshToken);
		}
	}

	private void ClearPrefs()
	{
		EditorPrefs.DeleteKey(ClientIdKey);
		EditorPrefs.DeleteKey(ClientSecretKey);
		EditorPrefs.DeleteKey(RedirectUriKey);
		EditorPrefs.DeleteKey(ScopeKey);
		EditorPrefs.DeleteKey(AccessCodeKey);
		EditorPrefs.DeleteKey(AccessTokenKey);
		EditorPrefs.DeleteKey(RefreshTokenKey);

		LoadPrefs();
	}

	#endregion
}
