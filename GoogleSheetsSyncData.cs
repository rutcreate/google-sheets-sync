﻿using UnityEngine;

using System;
using System.Collections.Generic;


[Serializable]
public class GoogleSheetsSyncData
{
	/// <summary>
	/// The sheet identifier.
	/// </summary>
	public string SheetId;

	/// <summary>
	/// The name of the sheet.
	/// </summary>
	public string SheetName;

	/// <summary>
	/// The skip first row.
	/// </summary>
	public bool SkipFirstRow;

	/// <summary>
	/// The titles.
	/// </summary>
	public List<string> Titles;

	/// <summary>
	/// The rows.
	/// </summary>
	public List<Row> Rows;

	/// <summary>
	/// Initializes a new instance of the <see cref="GoogleSheetsSyncData"/> class.
	/// </summary>
	public GoogleSheetsSyncData()
	{
		Titles = new List<string>();
		Rows = new List<Row>();
	}

	/// <summary>
	/// Gets the cell data.
	/// </summary>
	/// <returns>The cell data.</returns>
	/// <param name="title">Title.</param>
	/// <param name="row">Row index. Start with 0.</param>
	public string GetCellData(string title, int rowIndex)
	{
		int columnIndex = Titles.IndexOf(title);
		if (columnIndex < 0)
		{
			throw new ArgumentOutOfRangeException("title", "Could not find title.");
		}

		if (rowIndex >= Rows.Count)
		{
			throw new ArgumentOutOfRangeException("rowIndex", "Row index out of bound.");
		}

		Row row = Rows[rowIndex];
		if (row != null)
		{
			if (columnIndex >= row.Cells.Count)
			{
				Debug.LogWarningFormat("Could not find cell at {0} named \"{1}\"", rowIndex, title);
			}
			else
			{
				return row.Cells[columnIndex];
			}
		}

		return string.Empty;
	}

	/// <summary>
	/// Clear data.
	/// </summary>
	public void Clear()
	{
		Titles.Clear();
		Rows.Clear();
	}

	/// <summary>
	/// Gets the count.
	/// </summary>
	/// <value>The rows count.</value>
	public int RowCount
	{
		get { return Rows.Count; }
	}

	[System.Serializable]
	public class Row
	{
		public List<string> Cells;

		public Row()
		{
			Cells = new List<string>();
		}
	}
}
